# pv175-template

This project is a fork of `https://gitlab.ics.muni.cz/396462/pv175` with hardcoded defaults for Cloud2Metacentrum / New OpenStack. Unlike original project, this one creates whole lab (including router/network/subnet/FIP assignment).

usage:
```
 openstack stack create --parameter lab_size=105 -e pb007-env.json -t PB007-stack.yaml  pv175-temp
```
